package az.ingress.marketapprepo.repository;

import az.ingress.marketapprepo.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BranchRepository extends JpaRepository<Branch,Long> {

}
