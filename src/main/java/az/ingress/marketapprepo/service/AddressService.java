package az.ingress.marketapprepo.service;

import az.ingress.marketapprepo.dto.AddressRequest;
import az.ingress.marketapprepo.dto.AddressResponse;
import az.ingress.marketapprepo.dto.BranchResponse;
import az.ingress.marketapprepo.model.Address;
import az.ingress.marketapprepo.model.Branch;
import az.ingress.marketapprepo.repository.AddressRepository;
import az.ingress.marketapprepo.repository.BranchRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressService {
    public final AddressRepository addressRepository;
    private  final BranchRepository branchRepository;
    private final ModelMapper modelMapper;
    public AddressResponse create(Long branchId, AddressRequest request){
     Branch branch =    branchRepository.findById(branchId).orElseThrow(()-> new RuntimeException(String.format("Branch with id %s not found ",branchId)));
        Address address = modelMapper.map(request, Address.class);
        branch.setAddress(address);
        branchRepository.save(branch);
        return modelMapper.map(address,AddressResponse.class);
    }
    @Transactional
    public AddressResponse get(Long addressId){
        Address address = addressRepository.findById(addressId).orElseThrow(() ->
                new RuntimeException(String.format("Address with id %s not found ", addressId)));
        return modelMapper.map(address, AddressResponse.class);


    }
    public List<AddressResponse> getAll(){
        List<Address> addresses =addressRepository.findAll();
        List<AddressResponse> addressResponses = new ArrayList<>();
        addresses.forEach(address ->
                addressResponses.add(modelMapper.map(address,AddressResponse.class)));
        return addressResponses;
    }
    @Transactional
    public AddressResponse update(Long addressId,Long branchId,AddressRequest request){
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(()-> new RuntimeException(String.format("Branch with id %s not found",branchId)));
        Address address =addressRepository.findById(addressId)
                .orElseThrow(()->new RuntimeException(String.format("Address with id %s not found ",addressId)));
        address.setName(request.getName());
        addressRepository.save(address);
        return modelMapper.map(address,AddressResponse.class);

    }
    @Transactional
    public void delete(Long branchId,Long addressId) {
        Branch branch =branchRepository.findById(addressId)
                .orElseThrow(()-> new RuntimeException(String.format("Branch with id %S not found",branchId)));
        branch.setAddress(null);
        branchRepository.save(branch);
        addressRepository.deleteById(addressId);
    }
}
