package az.ingress.marketapprepo.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "market")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Market {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String type;

@OneToMany(mappedBy = "market", cascade = CascadeType.ALL)
        @Builder.Default
    List<Branch> branches= new ArrayList<>();

@ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "market_region",
    joinColumns = @JoinColumn(name = "market_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name ="region_id"  ,referencedColumnName = "id"))
    List<Region> regions =new ArrayList<>();
}
