package az.ingress.marketapprepo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "branch")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Branch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    Integer countOfEmployee;

    @JsonIgnore
    @ManyToOne()
    @ToString.Exclude
    Market market;

    @OneToOne(cascade = CascadeType.ALL)
    Address address;
}
