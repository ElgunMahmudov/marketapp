package az.ingress.marketapprepo.repository;

import az.ingress.marketapprepo.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Long> {
}
