package az.ingress.marketapprepo.controller;

import az.ingress.marketapprepo.dto.MarketRequest;
import az.ingress.marketapprepo.dto.MarketResponse;
import az.ingress.marketapprepo.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {
    private final MarketService marketService;

    @PostMapping
    public MarketResponse create(@RequestBody  @Valid MarketRequest request) {
        return marketService.create(request);

    }
    @GetMapping("/{marketId}")
    public MarketResponse get(@PathVariable Long marketId) {
        return marketService.get(marketId);
    }
    @DeleteMapping("/{marketId}")
    public void delete(@PathVariable Long marketId){
        marketService.delete(marketId);
    }

}
