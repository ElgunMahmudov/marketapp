package az.ingress.marketapprepo.controller;

import az.ingress.marketapprepo.dto.BranchRequest;
import az.ingress.marketapprepo.dto.BranchResponse;
import az.ingress.marketapprepo.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class BranchController {
    private final BranchService branchService;

    @PostMapping("/{marketId}")
    public BranchResponse create(@PathVariable Long marketId, @RequestBody BranchRequest request) {
        return branchService.create(marketId, request);
    }

    @PutMapping("/{marketId}/branch/{branchId}")
    public BranchResponse create(@PathVariable Long marketId,
                                 @PathVariable Long branchId,
                                 @RequestBody BranchRequest request) {
        return branchService.update(marketId, branchId, request);
    }

}
