package az.ingress.marketapprepo.repository;

import az.ingress.marketapprepo.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region,Long> {
}
