package az.ingress.marketapprepo.controller;

import az.ingress.marketapprepo.dto.RegionRequest;
import az.ingress.marketapprepo.dto.RegionResponse;
import az.ingress.marketapprepo.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/region")
public class RegionController {
    private final RegionService regionService;

    @PostMapping
    public RegionResponse create(@RequestBody RegionRequest request) {
        return regionService.create(request);
    }

    @GetMapping("/{regionId}")
    public RegionResponse get(@PathVariable Long regionId) {
        return regionService.get(regionId);
    }

    @GetMapping
    public List<RegionResponse> getAll() {
        return regionService.getAll();
    }

    @PutMapping("/{regionId}")
    public RegionResponse update(@PathVariable Long regionId, @RequestBody RegionRequest regionRequest) {
        return regionService.update(regionId, regionRequest);
    }

    @DeleteMapping("/{regionId}")
    public void delete(@PathVariable Long regionId) {
        regionService.delete(regionId);
    }

}



