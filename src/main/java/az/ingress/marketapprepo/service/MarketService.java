package az.ingress.marketapprepo.service;

import az.ingress.marketapprepo.dto.MarketRequest;
import az.ingress.marketapprepo.dto.MarketResponse;
import az.ingress.marketapprepo.model.Market;
import az.ingress.marketapprepo.model.Region;
import az.ingress.marketapprepo.repository.MarketRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.internal.bytebuddy.implementation.auxiliary.AuxiliaryType;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.error.Mark;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MarketService {
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public MarketResponse create(MarketRequest request) {
        Market market = modelMapper.map(request, Market.class);
        market = marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }
@Transactional
    public MarketResponse get(Long marketId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with %s not found", marketId)));
        return modelMapper.map(market, MarketResponse.class);


    }
    public List<MarketResponse> getAll(){
        List<Market> markets =  marketRepository.findAll();
        List<MarketResponse> marketResponses =  new ArrayList<>();
        markets.forEach(market ->
                marketResponses.add((modelMapper.map(market,MarketResponse.class))));
        return marketResponses;
    }
    @Transactional
    public MarketResponse update(Long marketId,MarketRequest marketRequest) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with  id %s not found ", marketId)));
        market.setName(marketRequest.getName());
        market.setType(marketRequest.getType());
        marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }
    public MarketResponse updateMarketWithRegion(Long marketId, Region region) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        market.getRegions().add(region);
        market = marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);

    }
    @Transactional
public void delete(Long marketId) {
    marketRepository.deleteById(marketId);
    }
}

