package az.ingress.marketapprepo.service;

import az.ingress.marketapprepo.dto.RegionRequest;
import az.ingress.marketapprepo.dto.RegionResponse;
import az.ingress.marketapprepo.model.Market;
import az.ingress.marketapprepo.model.Region;
import az.ingress.marketapprepo.repository.MarketRepository;
import az.ingress.marketapprepo.repository.RegionRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public RegionResponse create(RegionRequest regionRequest){
        Region region = modelMapper.map(regionRequest,Region.class);
        region=regionRepository.save(region);
        return  modelMapper.map(region,RegionResponse.class);
    }
    @Transactional
    public RegionResponse get(Long regionId) {
        Region region = regionRepository.findById(regionId).get();
        return modelMapper.map(region, RegionResponse.class);
    }
    public List<RegionResponse> getAll() {
        List<Region> regions = regionRepository.findAll();
        List<RegionResponse> regionResponses = new ArrayList<>();
        regions.forEach(region ->
                regionResponses.add(modelMapper.map(region, RegionResponse.class)));
        return regionResponses;
    }

    public RegionResponse update(Long regionId, RegionRequest regionRequest) {
        Region region = regionRepository.findById(regionId).get();
        region.setName(regionRequest.getName());
        Region saveRegion = regionRepository.save(region);
        return modelMapper.map(saveRegion, RegionResponse.class);
    }

    public void delete(Long regionId) {
        List<Market> markets = marketRepository.findAll();
        markets.forEach(
                market -> market.getRegions().removeIf(
                        region -> region.getId() == regionId));

        markets.forEach(market -> marketRepository.save(market));

        regionRepository.deleteById(regionId);

    }


}
