package az.ingress.marketapprepo.service;

import az.ingress.marketapprepo.dto.BranchRequest;
import az.ingress.marketapprepo.dto.BranchResponse;
import az.ingress.marketapprepo.model.Branch;
import az.ingress.marketapprepo.model.Market;
import az.ingress.marketapprepo.repository.BranchRepository;
import az.ingress.marketapprepo.repository.MarketRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchService {
    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public BranchResponse create(Long marketId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        Branch branch = modelMapper.map(request, Branch.class);
        branch.setMarket(market);
        market.getBranches().add(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }


    @Transactional
    public BranchResponse get(Long branchId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        return modelMapper.map(branch, BranchResponse.class);
    }

    public List<BranchResponse> getAll() {
        List<Branch> branches = branchRepository.findAll();
        List<BranchResponse> branchResponses = new ArrayList<>();
        branches.forEach(branch -> branchResponses.add(modelMapper.map(branch, BranchResponse.class)));
        return branchResponses;
    }
    @Transactional
    public BranchResponse update(Long marketId, Long branchId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));

        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        branch.setName(request.getName());
        branch.setCountOfEmployee(request.getCountOfEmployee());
        branchRepository.save(branch);
        return modelMapper.map(branch, BranchResponse.class);

    }
    @Transactional
    public void  delete(Long branchid){
        branchRepository.deleteById(branchid);

    }



    }


