package az.ingress.marketapprepo.repository;

import az.ingress.marketapprepo.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarketRepository extends JpaRepository<Market,Long> {
}
