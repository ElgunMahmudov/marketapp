package az.ingress.marketapprepo.controller;

import az.ingress.marketapprepo.dto.AddressRequest;
import az.ingress.marketapprepo.dto.AddressResponse;
import az.ingress.marketapprepo.model.Address;
import az.ingress.marketapprepo.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @PostMapping("/{branchId}")
    public AddressResponse create(@PathVariable Long branchId, @RequestBody AddressRequest request) {
        return addressService.create(branchId, request);
    }
}

