package az.ingress.marketapprepo;

import az.ingress.marketapprepo.model.*;
import az.ingress.marketapprepo.repository.AddressRepository;
import az.ingress.marketapprepo.repository.BranchRepository;
import az.ingress.marketapprepo.repository.MarketRepository;
import az.ingress.marketapprepo.repository.RegionRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.beans.Transient;

@SpringBootApplication
@RequiredArgsConstructor
public class MarketAppRepoApplication implements CommandLineRunner {
    private final MarketRepository marketRepository;
    private final AddressRepository addressRepository;
    private final BranchRepository branchRepository;
    private final RegionRepository regionRepository;


    public static void main(String[] args) {
        SpringApplication.run(MarketAppRepoApplication.class, args);
    }

    @Transactional
    @Override
    public void run(String... args) throws Exception {
//        Market Araz = marketRepository.findById(1L).get();
//        Market araz = marketRepository.findById(3L).get();
//        Region absheron = Region.builder()
//                .name(RegionType.ABSHERON)
//                .build();
//
//        Region merkez = Region.builder()
//                .name(RegionType.MERKEZ)
//                .build();
//        regionRepository.save(absheron);
//        regionRepository.save(merkez);
//
//        Araz.getRegions().add(absheron);
//        Araz.getRegions().add(merkez);
//        araz.getRegions().add(absheron);
//        araz.getRegions().add(merkez);
//        marketRepository.save(Araz);
//        marketRepository.save(araz);
//    }
//}
//
////        Market market = Market.builder()
////                .name("araz")
////                .type("supermarket")
////                .build();
////        marketRepository.save(market);
////        Branch korogluBranch = Branch.builder()
////                .name("ayna")
////                .countOfEmployee(123)
////                .market(market)
////                .build();
//        Branch ahmedliBranch = Branch.builder()
//               .name("gencliy")
//               .countOfEmployee(155)
//                .market(market)
//                .build();
//        market.getBranches().add(korogluBranch);
//        market.getBranches().add(ahmedliBranch);
//        Address address1 = Address.builder()
//               .name("ayna sultanovvva")
//               .build();
//        Address address2 = Address.builder()
//               .name("gencliyy")
//                .build();
//        korogluBranch.setAddress(address1);
//      ahmedliBranch.setAddress(address2);
//
//
//       marketRepository.save(market);
//
//    }
//}
    }
}